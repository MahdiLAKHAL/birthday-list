import React from 'react';
import data from './data';
import "./index.css";
import List from "./list";

function App() {
    const [people, setPeople] = React.useState(data);
 return (
    <main>
      <section className='container'>
      {/* how to take the new length after removing one */}
        <h3>{people.length} birthdays today</h3>
        <List people={people}>
        </List>
        <button onClick={() => setPeople([])}>clear all</button>
      </section>
    </main>
  );
}

export default App;