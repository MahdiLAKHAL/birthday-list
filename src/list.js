import React from 'react';
import data from './data';


function List({people}) {
  // const [people, setPeople] = React.useState(data);
  // function removeItem(id) {
  //   let newPeople = people.filter((person) => person.id !== id);
  //   setPeople(newPeople);
  // }
  return (
    <>
      {people.map((person) => {
        const { id, name, age, image } = person;
        return (
          <div className='person' key={id}>
            <img src={image} alt={name} />
            <h4>{name}</h4>
            <p>{age} years </p>
            {/* <button onClick={() => removeItem(id)}>remove</button> */}
          </div>
        );
      })}
    </>
  );
}

export default List ;
